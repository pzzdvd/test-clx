$(function () {

    var datatable = $('#data-table').DataTable({
        paging: false,
        searching: false,
        info: false,
    });

    var viewType = $("input[name='viewOptions']:checked").val();
    viewTypeShowHide(viewType);

    $("input[name='viewOptions']").on('change', function () {
        viewType = $("input[name='viewOptions']:checked").val();
        viewTypeShowHide(viewType);
    });

    $('input[name="name_of_your_radiobutton"]:checked').val()

    $('.settings').on('click', function (event) {
        var data = [];
        switch (event.target.id) {
            case 'presetting-1':
                data = {
                    'view': 'table'
                };
                break;
            case 'presetting-2':
                data = {
                    'active': 1,
                    'view': 'table'
                };
                break;
            case 'presetting-3':
                data = {
                    'surname': 'b',
                    'view': 'thumb'
                };
                break;
            case 'presetting-4':
                data = {
                    'active': 1,
                    'to': '19/10/2015 00:00:00',
                    'view': 'thumb'
                };
                break;
            case 'presetting-5':
                data = {
                    'active': 1,
                    'from': '15/10/2015 00:00:00',
                    'view': 'thumb'
                };
                break;
            case 'presetting-6':
                data = {
                    'active': 1,
                    'from': '10/10/2015 00:00:00',
                    'to': '19/10/2015 00:00:00',
                    'view': 'thumb'
                };
                break;
            case 'presetting-7':
                data = {
                    'name': 'p',
                    'view': 'thumb'
                };
                break;
        }

        $.ajax({
            url: '/user/show-user',
            dataType: 'json',
            data: data,
            method: 'POST'
        })
            .done(function (result) {
                if (viewType === 'thumb') {
                    renderThumb(result);
                } else if (viewType === 'table') {
                    datatable.destroy();
                    renderTable(result);
                    datatable = $('#data-table').DataTable({
                        paging: false,
                        searching: false,
                        info: false
                    });
                }
            })
            .fail(function () {
                alert('Si è verificato un errore nella richiesta di dati al server');
            })
    });
});

function viewTypeShowHide(value) {

    if (value === 'table') {
        $('#table').show();
        $('#thumb').hide();
    } else if (value === 'thumb') {
        $('#table').hide();
        $('#thumb').show();
    }
}

function renderTable(data) {

    $('div#table tbody').empty();

    $.each(data, function (id, dataRow) {
        $tr = document.createElement('tr');

        $.each(dataRow, function (id, data) {
            $td = document.createElement('td');
            if (data instanceof Object) {
                var lastLogin = moment(data.date, "YYYY-MM-DD HH:mm:ss.SSS");
                data = lastLogin.format("DD/MM/YYYY HH:mm:ss")
            }
            $td.append(data);
            $tr.appendChild($td);
        });

        $('div#table tbody').append($tr);
    });
}

function renderThumb(data) {

    $('div#thumb').empty();

    $.each(data, function (id, user) {

        var $img = $('<img>')
            .attr({
                'style': 'width: 100px; margin-bottom: 10px;',
                'src': user.picture,
                'alt': user.surname + ' ' + user.name,
            });

        var lastLogin = moment(user.last_login.date, "YYYY-MM-DD HH:mm:ss.SSS");

        var $cardBody = $('<div></div>')
            .append(
                $('<p></p>')
                    .addClass('card-text')
                    .append(
                        user.name + '<br>' +
                        user.surname + '<br>' +
                        lastLogin.format("DD/MM/YYYY HH:mm:ss")
                    )
            );

        var $card = $('<div></div>')
            .addClass('mg-auto pd-10 mb-4 card')
            .attr({style: 'width: 120px;'})
            .append($img)
            .append($cardBody);

        var $cardDiv = $('<div></div>')
            .addClass('col-2')
            .append($card);

        $('div#thumb').append($cardDiv);

    });

    $cardDiv = $('<div></div>').addClass('col');

    $('div#thumb').append($cardDiv);
}
