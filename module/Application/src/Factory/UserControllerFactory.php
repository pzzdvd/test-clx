<?php

namespace Application\Factory;

use Application\Controller\UserController;
use Application\Service\UserService;
use Application\Service\ImageService;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

class UserControllerFactory implements FactoryInterface
{

    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ) {
        return new UserController($container->get(UserService::class),
            $container->get(ImageService::class));
    }
}
