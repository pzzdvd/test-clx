<?php

namespace Application\Factory;

use Application\Service\ImageService;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;

class ImageServiceFactory implements FactoryInterface
{

    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ) {

        return new ImageService();
    }
}

