<?php

namespace Application\Factory;

use Application\Service\UserService;
use Application\Service\ImageService;
use Zend\ServiceManager\Factory\FactoryInterface;
use Interop\Container\ContainerInterface;
use Zend\Hydrator;

class UserServiceFactory implements FactoryInterface
{

    public function __invoke(
        ContainerInterface $container,
        $requestedName,
        array $options = null
    ) {
        $hydratorClass = new Hydrator\ClassMethods();
        $hydratorObject = new Hydrator\ObjectProperty();

        return new UserService($hydratorClass, $hydratorObject,
            $container->get(ImageService::class));
    }
}


