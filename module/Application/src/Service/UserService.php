<?php

namespace Application\Service;

use Application\Model\User;
use Zend\Hydrator;

class UserService
{

    protected $hydratorClass;
    protected $hydratorObject;
    protected $imageService;


    public function __construct(
        Hydrator\HydrationInterface $hydratorClass,
        Hydrator\HydrationInterface $hydratorObject,
        ImageService $imageService
    ) {
        $this->hydratorClass = $hydratorClass;
        $this->hydratorObject = $hydratorObject;
        $this->imageService = $imageService;
    }


    public function getUser($filters)
    {
        $users = $this->getAllUsers();
        $users = $this->castToStdArray($users);
        $users = $this->castToUser($users);

//TODO: fare refactoring
        if (null !== $filters['active']) {
            $users = $this->activeFilter($users, $filters['active']);
        }

        if (null !== $filters['from']) {
            $users = $this->fromFilter($users, $filters['from']);
        }

        if (null !== $filters['to']) {
            $users = $this->toFilter($users, $filters['to']);
        }

        if (null !== $filters['name']) {
            $users = $this->nameFilter($users, $filters['name']);
        }

        if (null !== $filters['surname']) {
            $users = $this->surnameFilter($users, $filters['surname']);
        }

        return $users;
    }


    /**
     * DO NOT MODIFY THIS METHOD
     * ONLY THIS METHOD CAN READ data.txt
     */
    protected function getAllUsers()
    {
        $file = 'data/data.txt';
        $data = json_decode(file_get_contents($file));

        return $data;
    }


    protected function castToStdArray($stdObjs)
    {
        $hydrator = $this->hydratorObject;

        return array_map(function ($user) use ($hydrator) {
            return $hydrator->extract($user);
        }, $stdObjs);
    }


    protected function castToUser($array)
    {
        $hydrator = $this->hydratorClass;

        return array_map(function ($item) use ($hydrator) {
            return $hydrator->hydrate($item, new User());
        }, $array);
    }


    protected function activeFilter($users, $value)
    {
        return array_filter($users, function ($item) use ($value) {
            return $item->active === (integer)$value;
        });
    }


    protected function fromFilter($users, $value)
    {
        return array_filter($users, function ($item) use ($value) {
            return $item->last_login > $value;
        });
    }


    protected function toFilter($users, $value)
    {
        return array_filter($users, function ($item) use ($value) {
            return $item->last_login < $value;
        });
    }


    protected function nameFilter($users, $value)
    {
        return array_filter($users, function ($item) use ($value) {
            return (substr($item->name, 0, strlen($value)) === $value);
        });
    }


    protected function surnameFilter($users, $value)
    {
        return array_filter($users, function ($item) use ($value) {
            return (substr($item->surname, 0, strlen($value)) === $value);
        });
    }


    public function resizeUserImages($users)
    {
        $imageService = $this->imageService;

        return array_map(function ($user) use ($imageService) {
            $newImageName = DIRECTORY_SEPARATOR . User::USER_PICTURE_100_DIR . basename($user->picture);
            $newImagePath = $_SERVER['DOCUMENT_ROOT'] . $newImageName;

            $image = $imageService->load($user->picture);
            $newImage = $imageService->resizeToWidth($image, 100);
            $imageService->save($newImage, $newImagePath);
            $user->picture = $newImageName;

            return $user;
        }, $users);
    }
}
