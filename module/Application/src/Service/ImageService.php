<?php

namespace Application\Service;


class ImageService
{

    public function __construct()
    {
    }


    public function load($imageName)
    {
        $imageInfo = getimagesize($imageName);
        $imageType = $imageInfo[2];

        switch ($imageType) {
            case IMAGETYPE_JPEG:
                return imagecreatefromjpeg($imageName);
                break;
            case IMAGETYPE_GIF:
                return imagecreatefromgif($imageName);
                break;
            case IMAGETYPE_PNG:
                return imagecreatefrompng($imageName);
                break;
        }

        return null;
    }


    public function save(
        $image,
        $imageName,
        $imageType = IMAGETYPE_JPEG,
        $compression = 100
    ) {
        switch ($imageType) {
            case IMAGETYPE_JPEG:
                imagejpeg($image, $imageName, $compression);
                break;
            case IMAGETYPE_GIF:
                imagegif($image, $imageName);
                break;
            case IMAGETYPE_PNG:
                imagepng($image, $imageName);
                break;
        }
    }


    public function resizeToWidth($image, $width)
    {
        $ratio = $width / $this->getWidth($image);
        $height = $this->getHeight($image) * $ratio;

        return $this->resize($image, $width, $height);
    }


    public function getWidth($image)
    {
        return imagesx($image);
    }


    public function getHeight($image)
    {
        return imagesy($image);
    }


    protected function resize($image, $width, $height)
    {
        $newImage = imagecreatetruecolor($width, $height);
        imagecopyresampled($newImage, $image, 0, 0, 0, 0, $width,
            $height, $this->getWidth($image), $this->getHeight($image));

        return $newImage;
    }
}


