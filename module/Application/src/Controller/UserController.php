<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the
 *            canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc.
 *            (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Application\BL\SimpleImage;
use Application\Service\ImageService;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;
use Zend\Filter\StaticFilter;
use Application\Service\UserService;


class UserController extends AbstractActionController
{

    protected $userService;
    protected $imageService;


    public function __construct(
        UserService $userService,
        ImageService $imageService
    ) {
        $this->userService = $userService;
        $this->imageService = $imageService;
    }


    public function indexAction()
    {
        return new ViewModel();
    }


    public function showUserAction()
    {

        $active = (null === $this->params()->fromPost('active')) ?
            null :
            StaticFilter::execute($this->params()->fromPost('active'),
                'Whitelist', ['list' => [1, 0]]);

        $from = StaticFilter::execute(
            StaticFilter::execute($this->params()->fromPost('from'),
                'StringTrim'),
            'StripTags');

        if (null !== $from) {
            $from = \DateTime::createFromFormat('d/m/Y H:i:s', $from);
        }

        $to = StaticFilter::execute(
            StaticFilter::execute($this->params()->fromPost('to'),
                'StringTrim'),
            'StripTags');

        if (null !== $to) {
            $to = \DateTime::createFromFormat('d/m/Y H:i:s', $to);
        }

        $name = StaticFilter::execute(
            StaticFilter::execute($this->params()->fromPost('name'),
                'StringTrim'),
            'StripTags');
        $surname = StaticFilter::execute(
            StaticFilter::execute($this->params()->fromPost('surname'),
                'StringTrim'),
            'StripTags');

        $view = StaticFilter::execute($this->params()->fromPost('view'),
            'Whitelist', ['list' => ['table', 'thumb']]);

        $filters = array(
            'active'  => $active,
            'from'    => $from,
            'to'      => $to,
            'name'    => $name,
            'surname' => $surname,
        );

        $result = $this->userService->getUser($filters);

//TODO: al momento viene eseguita sempre, da eseguire solo se non c'è l'immagine
        $result = $this->userService->resizeUserImages($result);

        return new JsonModel($result);
    }
}
