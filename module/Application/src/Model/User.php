<?php

namespace Application\Model;


class User implements UserInterface
{

    const USER_PICTURE_DIR = 'data' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR;
    const USER_PICTURE_100_DIR = 'img' . DIRECTORY_SEPARATOR . '100' . DIRECTORY_SEPARATOR;
    const USER_PICTURE_PUBLIC = 'public' . DIRECTORY_SEPARATOR;

    public $id;
    public $name;
    public $surname;
    public $active;
    public $last_login;
    public $picture;
    public $rating;


    public function getId()
    {
        return $this->id;
    }


    public function setId($id)
    {
        $this->id = $id;
    }


    public function getName()
    {
        return $this->name;
    }


    public function setName($name)
    {
        $this->name = $name;
    }


    public function getSurname()
    {
        return $this->surname;
    }


    public function setSurname($surname)
    {
        $this->surname = $surname;
    }


    public function getActive()
    {
        return $this->active;
    }


    public function setActive($active)
    {
        $this->active = $active;
    }


    public function getLastLogin()
    {
        return $this->last_login->format('d/m/Y H:i:s');
    }


    public function setLastLogin($lastLogin)
    {
        $this->last_login = \DateTime::createFromFormat('Y-m-d H:i:s',
            $lastLogin);
    }


    public function getPicture()
    {
        return $this->picture;
    }


    public function setPicture($picture)
    {
        $this->picture = $picture;
    }


    public function getRating()
    {
        return $this->rating;
    }


    public function setRating($rating)
    {
        $this->rating = $rating;
    }
}
 
