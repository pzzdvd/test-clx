<?php

namespace Application\Model;

interface UserInterface
{

    public function getId();


    public function setId($id);


    public function getName();


    public function setName($name);


    public function getSurname();


    public function setSurname($surname);


    public function getActive();


    public function setActive($active);


    public function getLastLogin();


    public function setLastLogin($lastLogin);


    public function getPicture();


    public function setPicture($picture);


    public function getRating();


    public function setRating($rating);
}
 