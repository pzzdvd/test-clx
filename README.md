# Users example for CLXeurope

## Introduction

This is an example application using the Zend Framework MVC layer and module
systems. 
## Installation using Composer

The easiest way to install this project is to use
[Composer](https://getcomposer.org/).  If you don't have it already installed,
then please install as per the [documentation](https://getcomposer.org/doc/00-intro.md).

Clone this project from git (bitbucket).

Then install it using composer:

```bash
$ cd test-clx
$ composer update  # update dependencies
$ composer install  # install 
$ composer development-enable  # enable development mode
```

Once installed, you can test it out immediately using PHP's built-in web server:

```bash
$ php -S 0.0.0.0:8080 -t public/ public/index.php
```

This will start the cli-server on port 8080, and bind it to all network
interfaces. You can then visit the site at http://localhost:8080/
- which will bring up Users example welcome page.

**Note:** The built-in CLI server is *for development only*.

